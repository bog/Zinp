EXEC=zinp
CXX=g++
CXXFLAGS=-std=c++11 -g -Wall
SRC=$(wildcard src/*.cpp)
OBJ=$(SRC:.cpp=.o)

$(EXEC) : $(OBJ)
	$(CXX) $(CXXFLAGS) $^ -o $@

clear :
	rm -rf src/*.o

mrproper: clear
	rm -rf $(EXEC)
